open System

// Day 1 - Part 1

let sampleInput = [
    199
    200
    208
    210
    200
    207
    240
    269
    260
    263
]

let input = 
    IO.File.ReadAllLines("day1_input1.txt")
    |> Seq.map int

let isIncrease (a, b) =
    b > a

let result1 = 
    input 
    |> Seq.pairwise
    |> Seq.filter isIncrease
    |> Seq.length


// Day 1 - Part 2

let result2 =
    input
    |> Seq.windowed 3
    |> Seq.map Seq.sum
    |> Seq.pairwise
    |> Seq.filter isIncrease
    |> Seq.length
