open System

// Day 5 - Part 1

type Coordinate =
    { X: int; Y: int }

type Segment = Coordinate list

type Grid = Map<Coordinate, int>    // just model a grid as a map of coordinates / counts

let sampleInput =
   "0,9 -> 5,9
    8,0 -> 0,8
    9,4 -> 3,4
    2,2 -> 2,1
    7,0 -> 7,4
    6,4 -> 2,0
    0,9 -> 2,9
    3,4 -> 1,4
    0,0 -> 8,8
    5,5 -> 8,2"

let input = 
    IO.File.ReadAllText("day5_input1.txt")

let split (seperator: string) (input: string) =   
    input.Split(seperator) |> List.ofArray

let trim (input: string) = input.Trim()

let parseCoordinate (input: string) =
    let coord = 
        input |> split "," 
        |> List.map int
    
    { X = coord.[0]; Y = coord.[1] }

let parseRow (input: string) =
    let points =
        input |> trim |> split " -> "
        |> List.map parseCoordinate
    points.[0], points.[1]

let isPlottable (p1, p2) =
    p1.X = p2.X || p1.Y = p2.Y
 
let getSegment (p1, p2) =
    let getRange x y = 
        if x < y then [x .. y] else [y .. x]
    // // plot vertical line
    if p1.X = p2.X then 
        getRange p1.Y p2.Y
        |> List.map (fun y -> { X = p1.X; Y = y })
    // plot horizontal line
    elif p1.Y = p2.Y then
        getRange p1.X p2.X
        |> List.map (fun x -> { X = x; Y = p1.Y })
    else failwith $"Unable to get segment with points ({p1.X},{p1.Y}); ({p2.X},{p2.Y})"


let updateGrid coor grid = 
    grid |> Map.change coor (fun v -> 
    match v with 
    | Some v -> Some (v + 1)
    | None -> Some 1 )


let plotSegment segment grid =
    segment
    |> List.fold (fun grid' point -> updateGrid point grid') grid

let parseInput (input: string) =
    input
    |> split "\n"
    |> List.map (trim >> parseRow)


let grid = Map.empty<Coordinate, int>
let result =
    parseInput input
    |> List.filter isPlottable    
    |> List.map getSegment
    |> List.fold (fun grid' segment -> plotSegment segment grid') grid
    |> Map.filter (fun k v -> v > 1)
    |> Map.count

result


// Day 5 - Part 2

let getDeltas p1 p2 =
    let dx = p1.X - p2.X |> abs  // get delta in x-axis between 2 points
    let dy = p1.Y - p2.Y |> abs  // get delta in y-axis between 2 points
    dx, dy

let isPlottable' (p1, p2) =
    let dx, dy = getDeltas p1 p2
    dx = 0 || dy = 0 || dx = dy

let getSegment' (p1, p2) =
    let getRange a b =                  // get ascending or descending range between a and b
        let range = [min a b .. max a b]
        if a < b then range 
        else List.rev range
    let xCoors = getRange p1.X p2.X     // get all x-coordinates
    let yCoors = getRange p1.Y p2.Y     // get all y-coordinates
    
    // decide what sort of segment this is
    match getDeltas p1 p2 with
    | dx, dy when dx = dy ->    // plot diagonal segment (isosceles right triangle)
        (xCoors, yCoors) ||> List.map2 (fun x y -> { X = x; Y = y})
    | dx, 0  ->                 // plot horizontal segment
        xCoors |> List.map (fun x -> { X = x; Y = p1.Y })
    | 0, dy  ->                 // plot vertical segment
        yCoors |> List.map (fun y -> { X = p1.X; Y = y })
    | _, _   ->
        failwith $"Unable to get segment with points ({p1.X},{p1.Y}); ({p2.X},{p2.Y})"    
    
let grid' = Map.empty<Coordinate, int>
let result' =
    parseInput input
    |> List.filter isPlottable'        
    |> List.map getSegment'    
    |> List.fold (fun grid'' segment -> plotSegment segment grid'') grid'
    |> Map.filter (fun k v -> v > 1)
    |> Map.count

result'

