open System


// Day 2 - part 1

let sampleInput = [
    "forward 5"
    "down 5"
    "forward 8"
    "up 3"
    "down 8"
    "forward 2"
]

let input = 
    IO.File.ReadAllLines("day2_input1.txt")

let parseCommand (command: string) =
    let parts = command.Split()
    let direction = parts.[0]
    let distance = Int32.Parse parts.[1]
    direction, distance

// Takes comand and returns tuple of horizontal, vertical movement
let getMovement  (command: string) =
    match parseCommand command with
    | "down", distance    -> 0, distance
    | "up", distance      -> 0, distance * -1    
    | "forward", distance -> distance, 0
    | _ -> failwith "error parsing instruction"


let getNewPosition pos mov =
    let h, v = pos
    let h', v' = mov
    h + h', v + v'

let result =
    input
    |> List.ofSeq
    |> List.map getMovement
    |> List.fold getNewPosition (0, 0)

let h, v = result
h * v

// Day 2 - part 1

// Takes tuple of (h, v, aim) and a command and returns tuple of new (h, v, aim)
let getNewPosition' pos command =
    let h, v, aim = pos
    match command with
    | "down", distance    -> h, v, aim + distance
    | "up", distance      -> h, v, aim - distance
    | "forward", distance -> h + distance, v + aim * distance, aim
    | _ -> failwith "error parsing instruction"   


let result2 =
    input
    |> List.ofSeq
    |> List.map parseCommand
    |> List.fold getNewPosition' (0, 0, 0)

let h', v', _ = result2

h' * v'