open System
open System.Text.RegularExpressions

// Day 4 - Part 1
let sampleInput = 
    """7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

    22 13 17 11  0
     8  2 23  4 24
    21  9 14 16  7
     6 10  3 18  5
     1 12 20 15 19

     3 15  0  2 22
     9 18 13 17  5
    19  8  7 25 23
    20 11 10 24  4
    14 21 16 12  6

    14 21 17 24  4
    10 16 15  9 19
    18  8 23 26 20
    22 11 13  6  5
     2  0 12  3  7"""
     
let input = 
    IO.File.ReadAllText("day4_input1.txt")

type Number =
    { Number: int
      Marked: bool }

type Board = Number list list

let split (seperator: string) (input: string) =   
    input.Split(seperator) |> List.ofArray

let createBoard (input: string) =
    let sanitize s = Regex.Replace(s, "\s+", " ").Trim()
    let createUnmarkedNumber n = { Number = Int32.Parse(n); Marked = false }
    let createRowNumbers (row: string) = 
        row 
        |> split " " 
        |> List.map createUnmarkedNumber
    
    // Create and return board from input
    input
    |> split "\n"   // split string into rows
    |> List.map (sanitize >> createRowNumbers ) // create list of numbers for each row
    

let printBoard (board: Board) =
    for row in board do
        for column in row do
            printf $" %i{column.Number}%c{if column.Marked then '*' else ' '}"
        printfn ""

let parseInput (input: string) =
    let head :: tail = input |> split "\n\n"
    let drawList = 
        head
        |> split ","
        |> List.map int

    let boards =
        tail
        |> List.map createBoard
    
    drawList, boards

// Mark number on board if exist and return updated board
let markNumber n board =  
    let updateRow row = 
        List.map (fun number -> 
            if number.Number = n then { Number = n; Marked = true }
            else number ) row
    
    board |> List.map updateRow

// Returns true if given board has won i.e. it 
// has a row or column of numbers marked true
let hasWon (board: Board) =
    let allMarked numbers = numbers |> List.forall (fun n -> n.Marked)
    let hasWinningRow = board |> List.exists allMarked
    let hasWinningColumn = board |> List.transpose |> List.exists allMarked
    hasWinningRow || hasWinningColumn

let getUnmarkedNumbers (board: Board) =
    board
    |> List.collect id  // flatten board into list of nums
    |> List.filter (fun n -> not n.Marked)
    |> List.map (fun n -> n.Number)
    

// Play bingo with given draw list and list of boards until
// no numbers remain in draw list or there are one or more 
// winning boards
let play drawList boards =
    let updateBoards number boards = List.map (markNumber number) boards

    let rec loop lastDraw drawList boards =
        match drawList with
        | [] -> lastDraw, drawList, boards
        | nextDraw :: remainingDraws ->
            printfn $"playing with {nextDraw}" 
            let updatedBoards = updateBoards nextDraw boards
            let winningBoards = List.filter hasWon updatedBoards
            if winningBoards.IsEmpty then
                loop nextDraw remainingDraws updatedBoards
            else
                nextDraw, remainingDraws, winningBoards
    
    let lastDraw, remainingDraws, winningBoards = loop 0 drawList boards
    lastDraw, winningBoards

    

let drawList, boards = parseInput sampleInput
let lastDraw, winningBoards = play drawList boards
printBoard winningBoards.[0]

let unmarkedSum = 
    getUnmarkedNumbers winningBoards.[0]
    |> List.sum

let result = unmarkedSum * lastDraw


// Day 4 - Part 2

// Play bingo with given draw list and list of boards until
// no numbers remain in draw list, and returns last winning 
// draw and winning boards
let playToEnd drawList boards =
    let updateBoards number boards = List.map (markNumber number) boards

    let rec loop lastWinningDraw lastWinners drawList boards =
        match drawList with
        | [] -> lastWinningDraw, lastWinners, drawList, boards
        | nextDraw :: remainingDraws ->
            printfn $"playing with {nextDraw}" 
            let updatedBoards = updateBoards nextDraw boards
            let allWinners = List.filter hasWon updatedBoards |> Set
            let allLosers = Set(updatedBoards) - allWinners |> List.ofSeq
            let newWinners = allWinners - lastWinners
            
            // No new winners, so preserve last winning draw and board
            if newWinners.IsEmpty then
                loop lastWinningDraw lastWinners remainingDraws allLosers
            // New winner(s), so pass winning draw and boards into next call
            else
                printfn $"Winning number was {nextDraw}, {newWinners.Count} new winners" 
                loop nextDraw newWinners remainingDraws allLosers
    
    let lastDraw, lastWinners, drawList, boards = loop 0 Set.empty drawList boards
    lastDraw, List.ofSeq lastWinners

let drawList', boards' = parseInput input
let lastDraw', lastWinners = playToEnd drawList' boards'

printBoard lastWinners.[0]

let unmarkedSum' = 
    getUnmarkedNumbers lastWinners.[0]
    |> List.sum

let result2 = unmarkedSum' * lastDraw'