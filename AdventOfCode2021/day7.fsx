open System


// Day 7 - Part 1

let sampleInput = "16,1,2,0,4,2,7,1,2,14"

let input = 
    IO.File.ReadAllText("day7_input.txt")

// Split string on given seperator and return list of tokens
let split (seperator: string) (input: string) =   
    input.Split(seperator) |> List.ofArray

// Parse horizontal start positions from input string
let parsePositions input =
    input |> split "," |> List.map int

// Get cost of moving from start to finish position (simple costing function)
let getCostSimple start finish =
    finish - start |> abs

// Returns cost of moving all starting positions to finish position using given cost function
let evaluateMove costFn positions finish =
    let cost = positions |> List.sumBy (fun p -> costFn p finish)
    finish, cost

// Get all possible finish positions for given list of start positions
let getPossiblePositions positions =
    let minPosition = List.min positions
    let maxPosition = List.max positions
    [minPosition .. maxPosition]

let positions = input |> parsePositions
let possiblePositions = positions |> getPossiblePositions

// Partially apply evaluateMove with simple costing function and starting positions
let evaluate = evaluateMove getCostSimple positions

// Evaluate cost of moving to all possible positions, and find position with min cost
possiblePositions
|> List.map evaluate
|> List.minBy (fun (pos, cost) -> cost)

// Day 7 - Part 2

// d0 = 0
// d1 = 1         = 1
// d2 = 1 + 2     = 3
// d3 = 1 + 2 + 3 = 6

// Get cost of moving from start to finish position (more complex costing function)
let getCostComplex start finish =
    let d = finish - start |> abs
    d * (d + 1) / 2     // use sum of arithmetic sequence

// Partially apply evaluateMove with more complex costing function and starting positions
let evaluate' = evaluateMove getCostComplex positions

// Evaluate cost of moving to all possible positions, and find position with min cost
possiblePositions
|> List.map evaluate'
|> List.minBy (fun (pos, cost) -> cost)
