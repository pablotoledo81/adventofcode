open System

// Day 3 - Part 1
let sampleInput = [
    "00100"
    "11110"
    "10110"
    "10111"
    "10101"
    "01111"
    "00111"
    "11100"
    "10000"
    "11001"
    "00010"
    "01010"
]

let input = 
    IO.File.ReadAllLines("day3_input1.txt")
    |> List.ofArray

let getCharList (s: string) =
    s.ToCharArray() |> List.ofSeq

let convertStringToDecimal s =
    Convert.ToInt32(s, 2)

let convertCharsToDecimal (chars: char list) =
    chars
    |> Array.ofList 
    |> String
    |> convertStringToDecimal

let mostCommonChar charList =
    let char, count =
        charList
        |> List.countBy id
        |> List.maxBy (fun (char, count) -> count)
    char

let leastCommonChar charList =
    let char, count =
        charList
        |> List.countBy id
        |> List.minBy (fun (char, count) -> count)
    char

let findInstruction (criteria: char list -> char) (instructions: string list)  =
    instructions
    |> List.map getCharList
    |> List.transpose
    |> List.map criteria
    |> convertCharsToDecimal

let gammaRate = 
    input |> findInstruction mostCommonChar

let epsilonRate = 
    input |> findInstruction leastCommonChar

let result = gammaRate * epsilonRate


// Day 3 - Part 2
let getBitCounts (bits: char list) =
    let bitCounts = List.countBy id bits
    let _, zeroBits = 
        bitCounts
        |> List.find (fun (char, _) -> char = '0')
    let _, oneBits = 
        bitCounts
        |> List.find (fun (char, _) -> char = '1')
    
    zeroBits, oneBits

let mostCommonBit bits =
    let zeroBits, oneBits = getBitCounts bits
    if oneBits > zeroBits then '1'
    elif oneBits < zeroBits then '0'
    else '1' 

let leastCommonBit bits =
    let zeroBits, oneBits = getBitCounts bits
    if oneBits > zeroBits then '0'
    elif oneBits < zeroBits then '1'
    else '0' 

let findRating (criteria: char list -> char) (rows: string list) =
    let rowChars = List.map getCharList rows

    // successively filter rows by each column until single row remains
    let rec loop column rows =
        let columnChars = List.transpose rows
        let filterByChar = criteria columnChars.[column]
        let filter (row: char list) = row.[column] = filterByChar
        let filteredRows = List.filter filter rows
        match filteredRows with
        | []    -> []   // should never happen!
        | [row] -> row  // base case - return single remaining row
        | _     -> loop (column + 1) filteredRows   // recurse on remaining rows
    
    loop 0 rowChars |> convertCharsToDecimal

let oxygenGeneratorRating =
    input |> findRating mostCommonBit

let co2ScrubberRating =
    input |> findRating leastCommonBit

let lifeSupportRating = oxygenGeneratorRating * co2ScrubberRating
