open System

// Day 6 - Part 1
type Population = Map<int, int64>
let sampleInput = "3,4,3,1,2"

let input = 
    IO.File.ReadAllText("day6_input.txt")

let split (seperator: string) (input: string) =   
    input.Split(seperator) |> List.ofArray

let (newPopulation: Population) =
    [0..8] 
    |> List.map (fun n -> n, 0L)
    |> Map.ofList

// Add given count to age for a population, returning updated population
let updatePopulation age count (population: Population) =
    population 
    |> Map.change age (fun value -> 
        match value with 
        | Some n -> Some (count + n)
        | None   -> Some count )

// Parse input and return population map of initial cohorts
let parseInput input =
    let cohorts =
        input 
        |> split ","
        |> List.map int
        |> List.countBy id
        |> List.map (fun (k, v) -> k, (int64) v)
        |> Map.ofList

    // Merge new (empty) population with initial cohorts
    Map.fold (fun s k v -> Map.add k v s) newPopulation cohorts

// Age individual of given age by one day
let ageOneDay currentAge = 
    match currentAge with 
    | 0 -> 6
    | n -> n - 1 

let getPopulationSummary population =    
    Map.fold (fun s k v -> sprintf $"{s}({k} {v}) ") "" population

// Age a population by a day, returning updated population
let agePopulation (population: Population) =
    // Add children equal to number of parents (at n=0)
    let newChildren = population |> Map.find 0
    
    // Age parents by one day
    let parents =
        Map.fold (fun population' age count -> 
            let parentAge = ageOneDay age
            population'
            |> updatePopulation age (count * -1L)    // substract count from this age
            |> updatePopulation parentAge count     // and add it to new age
        ) population population
    
    // Add new children (n=8) to population
    parents |> updatePopulation 8 newChildren


let rec evolve generations population =
    let rec loop n population =
        //printfn $"Gen {n}: {getPopulationSummary population}"
        if n = generations then population
        else agePopulation population |> loop (n + 1)
        
    loop 0 population


let getPopulationSize (population: Population) =
    population |> Map.fold (fun sum k v -> sum + v) 0L

let result =
    input
    |> parseInput
    |> evolve 80
    |> getPopulationSize

// Day 6 - Part 2    

let result2 =
    input
    |> parseInput
    |> evolve 256
    |> getPopulationSize

result2





