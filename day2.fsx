open System

type Move = Rock | Paper | Scissors
type Round = 
    { Move1: Move
      Move2: Move }
type Outcome = Win | Lose | Draw      

let sample = [|"A Y"; "B X"; "C Z"|]

/// Read text from file at given path
let readLines path =
    IO.File.ReadLines(path) |> Array.ofSeq

/// Takes valid first or second move letter and returns Move
let getMove = function
    | "A" | "X" -> Rock
    | "B" | "Y" -> Paper
    | "C" | "Z" -> Scissors
    | _ -> failwith "invalid move"

/// Takes array of lines and returns array of rounds 
let getRounds (lines: string array) = 
    lines
    |> Array.map ( fun line ->
        let moves = line.Split(" ")
        { Move1 = getMove moves.[0]
          Move2 = getMove moves.[1] } )

/// Takes a round and returns a tuple of outcomes for player1 and player2
let getRoundOutcome = function
    | { Move1 = a; Move2 = b } when a = b   -> Draw, Draw   // player 1 and 2 draw    
    | { Move1 = Scissors; Move2 = Rock }    -> Lose, Win    // player 2 wins
    | { Move1 = Paper; Move2 = Scissors }   -> Lose, Win    // player 2 wins
    | { Move1 = Rock; Move2 = Paper }       -> Lose, Win    // player 2 wins
    | _ -> Win, Lose    // player 1 wins
    
/// Get points for a single move
let getMovePoints = function
    | Rock      -> 1
    | Paper     -> 2
    | Scissors  -> 3

/// Get points for a round outcome
let getOutcomePoints = function
    | Lose  -> 0
    | Draw  -> 3
    | Win   -> 6

// Takes player's move and round outcome, and returns player's score
let getPlayerScore move outcome =
    getMovePoints move + getOutcomePoints outcome

/// Takes a round and returns tuple of scores for player1 and player2
let getRoundScores round = 
    let player1Outcome, player2Outcome = getRoundOutcome round
    let player1Score = getPlayerScore round.Move1 player1Outcome
    let player2Score = getPlayerScore round.Move2 player2Outcome
    
    player1Score, player2Score

/// Takes array of round scores and reduces to total score for player1 and player2
let getTotalScores (roundScores: (int * int) array) =
    roundScores
    |> Array.reduce ( fun (p1Total, p2Total) (p1, p2) ->
        p1Total + p1, p2Total + p2 )

// Part 1 - get player2's total score 
let player1TotalScore, player2TotalScore =
    readLines "day2.input.txt"
    |> getRounds
    |> Array.map getRoundScores
    |> getTotalScores


// Part 2 - first work out share required for desired outcome, and then get player2's total score

type DesiredOutcome =
    { Move1: Move
      Outcome: Outcome }

/// Takes valid second move and returns outcome
let getOutcome = function
    | "X"   -> Lose
    | "Y"   -> Draw
    | "Z"   -> Win
    | _ -> failwith "Invalid outcome"

/// Takes array of lines and returns array of desired outcomes
let getDesiredOutcomes (lines: string array) = 
    lines
    |> Array.map ( fun line ->
        let moves = line.Split(" ")
        { Move1 = getMove moves.[0]
          Outcome = getOutcome moves.[1] } )

let getWiningMove = function
    | Rock      -> Paper
    | Paper     -> Scissors
    | Scissors  -> Rock

let getLosingMove = function
    | Paper     -> Rock
    | Scissors  -> Paper
    | Rock      -> Scissors

let getMoveForDesiredOutcome = function
    | { Move1 = move; Outcome = Draw }  -> move    
    | { Move1 = move; Outcome = Win }   -> getWiningMove move
    | { Move1 = move; Outcome = Lose }  -> getLosingMove move


let computeRound (desiredOutcome: DesiredOutcome) =
    { Move1 = desiredOutcome.Move1 
      Move2 = getMoveForDesiredOutcome desiredOutcome } 

let player1TotalScore', player2TotalScore' =
    readLines "day2.input.txt"
    |> getDesiredOutcomes
    |> Array.map (computeRound >> getRoundScores)
    |> getTotalScores