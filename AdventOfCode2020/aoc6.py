import functools 

# reads puzzle file with given name and returns list of group answers
# e.g. [['a, ab, abc'], ['abc, cde']]
def load_group_answers(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().split('\n\n')

    return [r.split('\n') for r in rows]


# takes a list of group answers e.g. ['ab', 'ac', 'abd']
# and returns the set of any yes answers e.g. {'a', 'b', 'c', 'd'}
def get_any_yes_answer_set(group_answers):
    group_answers_set = set()
    for member_answers in group_answers:
        for answer in member_answers:
            group_answers_set.add(answer)

    return group_answers_set

# takes a list of group answers e.g. ['ab', 'ac', 'abd']
# and returns the set of every yes answers e.g. {'a'}
def get_every_yes_answer_set(group_answers):
    
    # function which takes string of member answers (e.g. 'abc')
    # and returns set of member answers (e.g. {'a', 'b', 'c'})
    def get_member_answer_set(member_answers):
        return set(([answer for answer in member_answers]))

    # transform group member answers list (e.g. ['ab', 'ac', 'abd'])
    # into list of member answer sets e.g. [{'a', 'b'}, {'a', 'c'}, {'a', 'b', 'd'}]
    group_answers = [get_member_answer_set(member_answers) for member_answers in group_answers]

    # intersect all member answer sets, leaving only the common answers
    common_answers = functools.reduce(lambda mas1, mas2: mas1 & mas2, group_answers)

    return common_answers

# load the demo group answers and find group any yes count
group_answers = load_group_answers('aoc6.demo.txt')
group_any_yes_count = sum(len(get_any_yes_answer_set(ga)) for ga in group_answers)
print(f"demo group any yes count = {group_any_yes_count}")

# now find demo group every yes count
for ga in group_answers:
    print(f"demo group answers = {ga}, group every yes set = {get_every_yes_answer_set(ga)}")

group_every_yes_count = sum(len(get_every_yes_answer_set(ga)) for ga in group_answers)
print(f"demo group every yes count = {group_every_yes_count}")

# load the input group answers and find group any yes count
group_answers = load_group_answers('aoc6.input.txt')
group_any_yes_count = sum(len(get_any_yes_answer_set(ga)) for ga in group_answers)
print(f"group any yes count = {group_any_yes_count}")

# now find demo group every yes count
group_every_yes_count = sum(len(get_every_yes_answer_set(ga)) for ga in group_answers)
print(f"group every yes count = {group_every_yes_count}")