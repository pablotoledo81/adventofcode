# reads puzzle file with given name and returns tree map as list of char lists
def load_tree_map(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().splitlines()

    return list(map(lambda r: [c for c in r], rows))

# returns contents of cell at x, y coordinate for given tree_map
# method assumes that pattern in any given map row repeats indefinitely
def get_cell_contents(tree_map, x, y):
    x_width = len(tree_map[0])
    y_height = len(tree_map)

    if x_width < 1 or y >= y_height:
        return None

    # do modulo-division on x so it falls within range 0..x_width
    x = x % x_width

    return tree_map[y][x]
    
# traverse given tree_map starting at top left
# until bottom edge is reached and return route
# as list of coordinates
def get_route(tree_map, right_step, down_step):
    max_bottom_index = len(tree_map) - 1
    x = 0
    y = 0
    route = []

    while y <= max_bottom_index:
        route.append((x, y))
        x = x + right_step
        y = y + down_step
    
    return route[1:]

# count number of trees for given tree map and route
def count_trees(tree_map, route):
    visited = []

    for x, y in route:
        visited.append(get_cell_contents(tree_map, x, y))

    return sum(map(lambda n: n == '#',visited))

tree_map = load_tree_map("aoc3.input.txt")

route = get_route(tree_map, 1, 1)
tree_count = count_trees(tree_map, route)
print(f"right 1, down 1 tree count = {tree_count}")

route = get_route(tree_map, 3, 1)
tree_count = count_trees(tree_map, route)
print(f"right 3, down 1 tree count = {tree_count}")

route = get_route(tree_map, 5, 1)
tree_count = count_trees(tree_map, route)
print(f"right 5, down 1 tree count = {tree_count}")

route = get_route(tree_map, 7, 1)
tree_count = count_trees(tree_map, route)
print(f"right 7, down 1 tree count = {tree_count}")

route = get_route(tree_map, 1, 2)
tree_count = count_trees(tree_map, route)
print(f"right 1, down 2 tree count = {tree_count}")


