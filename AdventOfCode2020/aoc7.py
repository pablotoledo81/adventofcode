import re

class Bag:
    def __init__(self, name):
        self.name = name
        self.rules = []
    

class Rule:
    def __init__(self, bag, qty):
        self.bag = bag
        self.qty = qty

# take a bag string and return a map in the form
# {name: name, rules: [{qty: qty, bag: bag1}, {qty: qty, bag: bag2}]}
def get_bag_map(bag_string):
    name = re.findall('^(\w+ \w+).*$', bag_string)[0]
    rule_list = re.findall('(\d \w+ \w+)', bag_string)
    rule_maps = [{'qty': int(rule[:1]), 'bag_name': rule[2:]} for rule in rule_list]

    return {
        'name': name,
        'rules': rule_maps
    }

# reads puzzle file with given name and returns a list of bags instances
def load_bags(fname):
    rows = []
    # read the rows
    with open(fname, "r") as f:
        rows = f.read().splitlines()
    
    # convert each row to a map representing a single bag 
    bag_maps = [get_bag_map(row) for row in rows]

    # create a list of bag objects (with no rules yet)
    bags = [Bag(b['name']) for b in bag_maps]

    # create a rule map with bag name as the key, 
    # and list of bag rules as the value
    rule_map = {b['name'] : b['rules'] for b in bag_maps}

    # add rules to each bag instance, linking each rule to another bag
    for bag in bags:
        rules = rule_map[bag.name]
        for rule in rules:
            # find bag with name which matches this rule
            other_bag = next(filter(lambda b: b.name == rule['bag_name'], bags))
            qty = rule['qty']
            bag.rules.append(Rule(other_bag, qty))

    return bags

# finds bag by name from list of bags
def find_bag(name, bags):
    return next(filter(lambda b: b.name == name, bags))

# returns a list of bags from the bag_list which must contain the given bag
def get_outer_bags(bag, bag_list):
    # returns true if bag allowed according to rules for b
    contains_bag = lambda b: bag in [r.bag for r in b.rules]
    
    return list(filter(contains_bag, bag_list))

# takes a bag and returns the set of possible colours of outer bags
# n.b a set is used to prevent bag colours being counted more than once
def get_all_outer_bags(bag, bag_list):
    outer_bags = get_outer_bags(bag, bag_list)
    
    # this bag has outer bags, so add those to the set and recurse on outer bags of each outer bag 
    if outer_bags:
        return set(outer_bags).union(*[get_all_outer_bags(ob, bag_list) for ob in outer_bags])

    # base case - no more outer bags so return empty set
    else:
        return {}


# takes a bag and returns the sum of all the inner bags it must contain
def count_inner_bags(bag):
    # this bag has inner bags, so recurse
    if bag.rules:
        # count all the bags contained in this bag
        bag_count = sum(r.qty for r in bag.rules)
        
        # recurse on bags contained in this bag and add to bag count
        inner_bag_count = sum(r.qty * count_inner_bags(r.bag) for r in bag.rules)
        
        return bag_count + inner_bag_count

    # base case: this bag contains no other bags so return 0
    else:
        return 0



# load demo input and count all outer bags
bags = load_bags('aoc7.demo.txt')
gold_bag = find_bag('shiny gold', bags)
outer_bags = get_all_outer_bags(gold_bag, bags)
print(f"{len(outer_bags)} outer bags for shiny gold = {[ob.name for ob in outer_bags]}")
print(f"inner bags = {count_inner_bags(gold_bag)}")


# load puzzle input and get all outer bags
bags = load_bags('aoc7.input.txt')
gold_bag = find_bag('shiny gold', bags)
outer_bags = get_all_outer_bags(gold_bag, bags)
print(f"{len(outer_bags)} outer bags for shiny gold")
print(f"inner bags = {count_inner_bags(gold_bag)}")