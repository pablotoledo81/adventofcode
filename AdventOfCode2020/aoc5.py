import re

# reads puzzle file with given name and returns tree map as list of char lists
def load_tickets(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().splitlines()

    return rows

# converts string s to a number where s specifies a series of binary partitions,
# nums is a list of numbers
# lower specifies the lower partition character
# upper specifies the upper partition character
# e.g. decode_partitions('FBF', 8, 'F', 'B') gives 2
def decode_partitions(s, nums, lower, upper):
    #print(f"s={s}, nums={nums}")
    
    # base case - return last value left in nums list
    if len(nums) == 1:
        return nums[0]
    
    # partition num list in half and recurse
    else:
        partition_type = s[0]
        partition_size = len(nums) // 2
        
        # keep lower partition
        if partition_type == lower:
            nums = nums[:partition_size]
        
        # keep upper partition
        elif partition_type == upper:
            nums = nums[partition_size:]

        # should never happen - return none
        else:
            return None
        
        return decode_partitions(s[1:], nums, lower, upper)

# decode boarding pass and returns a tuple in the form (row, column, seat_id)
def decode_boarding_pass(boarding_pass):
    # return none if invalid ticket ref
    if not re.match('^[F|B]{7}[L|R]{3}$', boarding_pass):
        return None
    
    # get row and column parts of ref and decode
    row_part = boarding_pass[:7]
    column_part = boarding_pass[7:]
    row_num = decode_partitions(s = row_part, nums = list(range(128)), lower = 'F', upper = 'B')
    column_num = decode_partitions(s = column_part, nums = list(range(8)), lower = 'L', upper = 'R')
    
    return (row_num, column_num, 8 * row_num + column_num)

# some tests
print(f"ticket 'FBFBBFFRLR' decodes to {decode_boarding_pass('FBFBBFFRLR')}")
print(f"ticket 'BFFFBBFRRR' decodes to {decode_boarding_pass('BFFFBBFRRR')}")
print(f"ticket 'FFFBBBFRRR' decodes to {decode_boarding_pass('FFFBBBFRRR')}")
print(f"ticket 'BBFFBBFRLL' decodes to {decode_boarding_pass('BBFFBBFRLL')}")

# load the puzzle input file and decode the tickets
tickets = load_tickets('aoc5.input.txt')
decoded_tickets = list(map(lambda t: decode_boarding_pass(t), tickets))
ticket_ids = set([seat_id for row, column, seat_id in decoded_tickets])

# get the max ticket id
max_seat_id = max(ticket_ids)
print(f"max seat id for puzzle input = {max_seat_id}")

# find the missing ticket id
missing_ids = [id for id in range(85, 891) if id not in ticket_ids]
print(f"missing id = {missing_ids[0]}")
