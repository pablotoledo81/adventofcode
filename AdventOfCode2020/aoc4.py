import re

# gets passport map from given passport string
def get_passport_map(passport_string):
    passport_map = {}
    
    # remove newline chars
    passport_string = passport_string.replace('\n', ' ')
    
    # get list of key-value pairs
    pairs = [pair.split(':') for pair in passport_string.split(' ')]

    # add pairs to map (ignore if value is null)
    for k, v in pairs:
        if v:
            passport_map[k] = v
    
    return passport_map
    

# reads puzzle file with given name and list of passport maps
def load_passport_maps(fname):
    passport_strings = []
    with open(fname, "r") as f:
        passport_strings = f.read().split('\n\n')

    return list(map(lambda p: get_passport_map(p), passport_strings))

# returns true if string s is an int and within given range, else returns  false
def is_in_range(s, min, max): 
    if not s.isnumeric():
        return False
    
    x = int(s)
    return x >= min and x <= max 

def passport_field_valid(name, value):

    if (name == 'byr'):
        return is_in_range(value, 1920, 2002)

    if (name == 'iyr'):
        return is_in_range(value, 2010, 2020)

    if (name == 'eyr'):
        return is_in_range(value, 2020, 2030)
    
    if (name == 'hgt'):
        parts = re.findall('(\d+)(in|cm)', value)
        
        # if value doesn't match regex, return false
        if not parts:
            return False
        
        # value passed regex, extract group contents
        height = parts[0][0]
        units = parts[0][1]
        valid_inches = units == 'in' and is_in_range(height, 59, 76)
        valid_cms = units == 'cm' and is_in_range(height, 150, 193)

        return valid_inches or valid_cms

    if (name == 'hcl'):
        return re.match('^#[0-9a-f]{6}$', value)

    if (name == 'ecl'):
        return value in {'amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth'}

    if (name == 'pid'):
        return re.match('^\d{9}$', value)

    if (name == 'cid'):
        return True

    
    


# returns true if passport contains required fields, else returns false
# if validate is set to true method will perform additional validation
def is_passport_valid(passport_map, validate):
    keys = set(passport_map.keys())
    required_keys = ['byr', 'iyr', 'eyr', 'hgt', 'hcl', 'ecl', 'pid']
    
    has_required_keys = all(map(lambda rk: rk in keys, required_keys))

    if validate and has_required_keys:
        has_valid_values = all([passport_field_valid(k, v) for k, v in passport_map.items()])
        return has_required_keys & has_valid_values
    
    else:
        return has_required_keys


def count_valid_passports(passport_maps, validate):
    return sum(map(lambda p: is_passport_valid(p, validate), passport_maps))

passport_maps = load_passport_maps('aoc4.input.txt')


print(f"Passports with required fields (no validation) = {count_valid_passports(passport_maps, False)}")

print(f"Passports with required fields (validated) = {count_valid_passports(passport_maps, True)}")
