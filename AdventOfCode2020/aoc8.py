import copy

class Instruction:  
    def __init__(self, line, op_name, op_arg):
        self.line = line
        self.op_name = op_name
        self.op_arg = op_arg

# reads puzzle file with given name and returns list of insruction objects
def load_instructions(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().splitlines()

    instructions = []
    row_indexes = range(0, len(rows))
    
    # create instruction object for each row in rows
    for i in row_indexes:
        row = rows[i]
        parts = row.split() 
        instruction = Instruction(i, parts[0], int(parts[1]))
        instructions.append(instruction)

    return instructions

# run list of instructions until all instructions executed 
# or instruction is run more than once, and return run result
def run_instructions(instructions):

    def process_instruction(ins):
        i_delta = 1                         # the index delta e.g. -1 or +3 (default = 1 i.e. the next op)
        acc_delta = 0                       # the accumulator value delta e.g. +2 (default = 0)

        if ins.op_name == 'jmp':
            i_delta = ins.op_arg
        
        if ins.op_name == 'acc':
            acc_delta = ins.op_arg

        return {'i_delta': i_delta, 'acc_delta': acc_delta}

    acc_value = 0       # the accumulator value
    i = 0               # the instruction index (start at 0)
    visited = set()     # the set of indexes which have been visited
    processed = []      # the list of processed instructions
    
    # process until we hit an instruction which was already
    # processed, or we process the last instruction
    while  i not in visited and i < len(instructions):
        ins = instructions[i]
        result = process_instruction(ins)
        processed.append(ins)
        visited.add(i)
        i = i + result['i_delta']
        acc_value = acc_value + result['acc_delta']

    return {'processed': processed, 'acc_value': acc_value, 'next_line': i}

# takes a run_result and returns an infinite instruction loop,
# (a list of instructions which run forever) or an empty list of no loop found
def find_infinite_loop(run_result):
    processed = run_result['processed']
    processed_lines = [l.line for l in processed]
    next_line = run_result['next_line']

    # no infitite loop found, program finished normally
    if next_line not in processed_lines:
        return []

    # find index of repeated line
    repeated_line = processed_lines.index(next_line)
    instruction_loop = processed[repeated_line:]

    return instruction_loop

# takes a list of instructions, and infinite loop
# and returns first fix which results in program completing normally
def find_program_fix(instructions, loop):
    if loop:
        candidates = list(filter(lambda i: i.op_name != 'acc', loop))
        for c in candidates:
            # copy instruction list, find candidate ins and change nop > jmp or jmp > nop
            new_instructions = copy.deepcopy(instructions)
            new_ins = next(filter(lambda i: i.line == c.line, new_instructions))
            new_ins.op_name = 'nop' if c.op_name == 'jmp' else 'jmp'
            
            # run instructions and see if result is still infinite loop
            results = run_instructions(new_instructions)
            loop = find_infinite_loop(results)
                    
            # if no loop then we've found the fix - return fixed instruction
            if not loop:
                print(f"fix found! - change line {new_ins.line} to a {new_ins.op_name}")
                print(f"acc value will be {results['acc_value']} after program terminates")
                return new_ins

    
    else:
        print("no loop found, program finished successfully")
        return None


# load and run instructions
instructions = load_instructions('aoc8.input.txt')
results = run_instructions(instructions)

# print("instructions:")
# for i in instructions:
#     print(f"line={i.line}, op={i.op_name}, arg={i.op_arg}")

# print("processed instructions:")
# for p in results['processed']:
#     print(f"line={p.line}, op={p.op_name}, arg={p.op_arg}")

print(f"processed {len(results['processed'])} instructions, accumulator value is {results['acc_value']}, next line is {results['next_line']}")

# look for infinite loop in run results
loop = find_infinite_loop(results)

if loop:
    print("program has infinite loop:")
    for p in loop:
        print(f"line={p.line}, op={p.op_name}, arg={p.op_arg}")

    # try to find fix for infinite loop
    fix = find_program_fix(instructions, loop)

    print(f"program fix: line {fix.line} should be {fix.op_name}")






