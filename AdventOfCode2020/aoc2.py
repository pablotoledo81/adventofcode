import re

# get password map from input string using regex
def get_password_map(input):
    parts = re.findall(r"(\d+)-(\d+) (\w): (\w+)", input)[0]
    return {
        "min_char": int(parts[0]),
        "max_char": int(parts[1]),
        "char": parts[2],
        "password": parts[3],
    }

# returns true if password in password map is valid, else returns  false
def is_password_valid(password_map):
    password = password_map['password']
    char = password_map['char']
    min_char = password_map['min_char']
    max_char = password_map['max_char']
    char_count = password.count(char)

    return char_count >= min_char and char_count <= max_char


# reads puzzle file with given name and returns list of password maps
def load_password_maps(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().splitlines()
        
    return list(map(lambda r: get_password_map(r), rows))


passwords = load_password_maps("aoc2.input.txt")

valid_passwords = sum(map(lambda p: is_password_valid(p), passwords))
print(f"{valid_passwords} valid passwords")