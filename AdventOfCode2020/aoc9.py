from aoc1 import binary_search
from aoc1 import find_addends

# reads puzzle file with given name and returns list
def load_nums(fname):
    rows = []
    with open(fname, "r") as f:
        rows = f.read().splitlines()

    return list(map(lambda r: int(r), rows))


# takes a list of nums and a preamble length p 
# and returns a list of nums for which there 
# are no addends in the previous p numbers
def find_encoding_errors(nums, p):
    check_nums = range(p, len(nums))   # the range of all the nums after the first p numbers which we'll check
    errors = []                        # list of nums which have been incorrectly encoded 

    # for each number n we're checking, find the preamble
    # (the last p numbers) and try to find 2 addends which sum to n
    for i in check_nums:
        n = nums[i] 
        preamble = sorted(nums[i - p : i])
        addends = find_addends(preamble, n)
        if addends:
            pass
            #print(f"n = {n}, preamble = {preamble}, addends = {addends}")
        else:
            errors.append(n) 
    
    return errors

# return list of contiguous addends for e from given nums list
# or empty list if none found
def find_addends_for_e(e, nums):
    addends = []
    total = 0

    # start at first num and keep adding until we hit or exceed e
    if nums:
        for i in range(len(nums)):
            n = nums[i]
            total = total + n
            addends.append(n)
            # sum of addends totals e, return list
            if total == e:
                return addends
            # sum of addends exceeds e, recurse on next num in list
            elif total > e:
                return find_addends_for_e(e, nums[1:])
    
    # base case - no nums so no addends exist
    else:
        return []



    return None


nums = load_nums('aoc9.demo.txt')
errors = find_encoding_errors(nums, 5)
print(f"incorrectly encoded numbers = {errors}")

nums = load_nums('aoc9.input.txt')
errors = find_encoding_errors(nums, 25)
print(f"incorrectly encoded numbers = {errors}")

addends_for_e = find_addends_for_e(90433990, nums)
min_addend = min(addends_for_e)
max_addend = max(addends_for_e)
print(f"addends for e = {addends_for_e}, min addend = {min_addend}, max addend = {max_addend}, sum = {min_addend + max_addend}")

