
# reads puzzle file with given name and returns sorted list of ints
def get_ints(fname):
	rows = []
	with open(fname, "r") as f:
		rows = f.read().splitlines()
		
	return sorted(list(map(lambda r: int(r), rows)))

# perform binary search on given array
def binary_search(array, element, start, end):
    if start > end:
        return -1

    mid = (start + end) // 2
    if element == array[mid]:
        return mid

    if element < array[mid]:
        return binary_search(array, element, start, mid-1)
    else:
        return binary_search(array, element, mid+1, end)

# find 2 addends from given num_list which sum to given total
def find_addends(num_list, total):
	max_index = len(num_list) - 1

	# iterate over each number in list
	for i in range(0, max_index - 1):
		x = num_list[i]

		# look for value y in list which is needed to make total
		y = total - x

		# if y found in list, return tuple of x and y
		if binary_search(num_list, y, i+1, max_index) != - 1:
			return (x, y)


# find n addends from given num_list which sum to given total
def find_n_addends(n, num_list, total):
	max_index = len(num_list) - 1

	# if n > 2, consider each number in the list and recurse on remaining list
	if n > 2:
		max_index = len(num_list) - 1
		for i in range(0, max_index):
			
			# assume z is an addend and calculate new total for remaining addends
			z = num_list[i]
			new_total = total - z

			# recurse on rest of list and new total
			addends = find_n_addends(n - 1, num_list[i+1:], new_total)
			
			# if addends found, add z to addend tuple and return
			if addends:
				return (z,) + addends
	
	# base case - find 2 addends which sum to given total 
	else:
		return find_addends(num_list, total)

# read integers from puzzle file
num_list = get_ints("aoc1.input.txt")

print(f"2 addends which sum to 2020: {find_n_addends(2 ,num_list, 2020)}")

print(f"3 addends which sum to 2020: {find_n_addends(3, num_list, 2020)}")

#print(f"4 addends which sum to 4553: {find_n_addends(4, num_list, 4553)}")

#print(f"5 addends which sum to 6499: {find_n_addends(5, num_list, 6499)}")