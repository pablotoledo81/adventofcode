open System

let sample = 
  [|"vJrwpWtwJgWrhcsFMMfFFhFp"
    "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
    "PmmdzqPrVvPwwTWBwg"
    "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
    "ttgJtRGJQctTZtZT"
    "CrZsJsPPZsGzwwsLwLmpwMDw"|]

/// IO
let readLines path = 
    IO.File.ReadAllLines path

/// Domain types
type Rucksack = 
    { Compartment1: string
      Compartment2: string }
    
    /// Returns contents of both compartments in rucksack
    member x.Contents = 
        x.Compartment1 + x.Compartment2

type Group =
    { Rucksack1: Rucksack 
      Rucksack2: Rucksack
      Rucksack3: Rucksack  }

/// Pure functions

/// Takes array of string and returns array of Rucksacks 
/// with items split into each compartment
let getRucksacks (lines: string array) =
    lines
    |> Array.map ( fun line ->
        let splitIndex = (line.Length / 2) - 1
        { Compartment1 = line.[..splitIndex]
          Compartment2 = line.[(splitIndex + 1)..] } )

/// Takes rucksack and returns item common to both compartments
/// (NB assumes that both compartments have exactly 1 item in common)
let getCommonItem rucksack =
    let set1 = rucksack.Compartment1 |> Set.ofSeq
    let set2 = rucksack.Compartment2 |> Set.ofSeq
    
    Set.intersect set1 set2
    |> List.ofSeq
    |> List.head

/// Map of item priorities
let itemPriorityMap =    
    ['a'..'z'] @ ['A'..'Z']
    |> List.mapi ( fun i c -> c, i + 1 )
    |> Map.ofList

/// Takes char c (which must be a valid item) and returns priority
let getPriority c =
    Map.find c itemPriorityMap

/// Takes array of rucksacks and returns array of 3-elf groups
let getGroups rucksacks =
    rucksacks
    |> Array.chunkBySize 3
    |> Array.map ( fun r ->
        { Rucksack1 = r.[0]
          Rucksack2 = r.[1]
          Rucksack3 = r.[2] } )

/// Takes a group and returns item common to all 3 rucksacks
let getGroupBadge group =
    let r1Contents = group.Rucksack1.Contents |> Set.ofSeq
    let r2Contents = group.Rucksack2.Contents |> Set.ofSeq
    let r3Contents = group.Rucksack3.Contents |> Set.ofSeq

    Set.intersectMany [r1Contents; r2Contents; r3Contents]
    |> List.ofSeq
    |> List.head


/// Root

/// Parse input to array of rucksacks
let rucksacks = 
    readLines "day3.input.txt"
    |> getRucksacks 

/// Part 1 - sum the priorities of common items
rucksacks
|> Array.map (getCommonItem >> getPriority)
|> Array.sum

// answer: 8240

/// Part 2 - Find the item type that corresponds to the badges of each three-Elf group.
rucksacks
|> getGroups
|> Array.map (getGroupBadge >> getPriority)
|> Array.sum

// answer: 2587