open System

let sample = 
    [|"30373"
      "25512"
      "65332"
      "33549"
      "35390"|]

let readLines path =    
    IO.File.ReadAllLines(path)

/// Takes string array and converts to 2d grid of ints
let getGrid (lines: string array) =
    lines
    |> Array.map ( fun line -> 
        line.ToCharArray() 
        |> Array.map ( fun n -> n.ToString() |> int ))
    |> array2D
    
/// Get row and column from grid for given coordinates
let getRowAndCol x y (grid: int[,]) =
    grid[y, *], grid[*, x]  // weirdly, we have to access an element in a 2d array with array[y,x]

/// gets list of coordinates for given grid
let getCoordinates grid =
    let xDim = grid |> Array2D.length2
    let yDim = grid |> Array2D.length1

    [for x in 0..xDim - 1 do 
        for y in 0..yDim - 1 -> x, y ]

let incIndex i = i + 1
let decIndex i = i - 1        

/// Returns true if tree at given coordinates is visible from top, bottom, left or right of grid 
let isTreeVisible x y (grid: int[,])  =   
    let treeHeight = grid[y, x]
    let treeRow, treeCol = getRowAndCol x y grid
    
    let rec visibleFromEdge (row: int array) getNextIdx idx =
        if idx = 0 || idx = row.Length - 1 then true
        else 
            let nextIdx = getNextIdx idx
            let visibleFromNext = row[nextIdx] < treeHeight
            visibleFromNext && visibleFromEdge row getNextIdx nextIdx
    
    let visibleFromLeft   = visibleFromEdge treeRow decIndex
    let visibleFromRight  = visibleFromEdge treeRow incIndex
    let visibleFromTop    = visibleFromEdge treeCol decIndex
    let visibleFromBottom = visibleFromEdge treeCol incIndex

    visibleFromLeft x || visibleFromRight x || visibleFromTop y || visibleFromBottom y  

/// Returns the scenic score of the tree at the given coordinates
let getScenicScore x y (grid: int[,])  =   
    let treeHeight = grid[y, x]
    let treeRow, treeCol = getRowAndCol x y grid
    
    let rec getScore score (row: int array) getNextIdx idx =
        if idx = 0 || idx = row.Length - 1 then score
        else 
            let nextIdx = getNextIdx idx
            if row[nextIdx] >= treeHeight then score + 1
            else getScore (score + 1) row getNextIdx nextIdx
    
    let getLeftScore   = getScore 0 treeRow decIndex
    let getRightScore  = getScore 0 treeRow incIndex
    let getTopScore    = getScore 0 treeCol decIndex
    let getBottomScore = getScore 0 treeCol incIndex

    (getLeftScore x) * (getRightScore x) * (getTopScore y) * (getBottomScore y)

// Part 1 - Consider your map; how many trees are visible from outside the grid?
let grid = readLines "day8.input.txt" |> getGrid

grid 
|> getCoordinates
|> List.map ( fun (x, y) -> isTreeVisible x y grid )
|> List.filter id
|> List.length

// answer: 1676

// Part 2 - Consider each tree on your map. What is the highest scenic score possible for any tree?
grid
|> getCoordinates
|> List.map ( fun (x, y) -> getScenicScore x y grid )
|> List.max

// answer: 313200
