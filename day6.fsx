open System

let sample0 = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"  // should be 7
let sample1 = "bvwbjplbgvbhsrlpgdmjqwftvncz"    // should be 5
let sample2 = "nppdvjthqldpwncqszvftbrmjlhg"    // should be 6
let sample3 = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"   // should be 10
let sample4 = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"    // should be 11


/// Read text from file at given path
let readInput path =
    IO.File.ReadAllText(path)

/// Return true if list of n chars is marker
let isMarker (n: int) (chars: char list) = 
    chars |> Set.ofList |> Set.count = n

/// Find first marker of length n in data stream and return char number
let findMarker (n: int) (dataStream: string) =
    // get list of sliding windows
    let windows = 
        dataStream
        |> List.ofSeq
        |> List.windowed n
    
    // find index of first window which is marker
    windows 
    |> List.findIndex (isMarker n)
    |> (+) n


// Part 1 - How many characters need to be processed before the first start-of-packet marker is detected?
readInput "day6.input.txt"
|> findMarker 4

// answer: 1343

// Part 2 - How many characters need to be processed before the first start-of-message marker is detected?
readInput "day6.input.txt"
|> findMarker 14

// answer: 2193