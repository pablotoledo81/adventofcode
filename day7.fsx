open System
open System.Text.RegularExpressions

let sample = 
  [ "$ cd /"
    "$ ls"
    "dir a"
    "14848514 b.txt"
    "8504156 c.dat"
    "dir d"
    "$ cd a"
    "$ ls"
    "dir e"
    "29116 f"
    "2557 g"
    "62596 h.lst"
    "$ cd e"
    "$ ls"
    "584 i"
    "$ cd .."
    "$ cd .."
    "$ cd d"
    "$ ls"
    "4060174 j"
    "8033020 d.log"
    "5626152 d.ext"
    "7214296 k" ]

type File =
    { FileName: string
      FileSize: int64
      Path: string }

type Directory =
    { DirectoryName: string
      SubDirectories: Directory list
      Files: File list
      TotalSize: int64
      Path: string }  

type FileOrDir =
    | File of File
    | Directory of Directory

// IO

let readLines path = 
    IO.File.ReadAllLines path
    |> List.ofArray

// Pure functions

/// Takes root and dir name and returns new parent or child path
let buildPath root dirName =
    match root, dirName with
    | "/", ".."    -> "/"                   // cd up from / gives you /
    | "/", "/"     -> "/"                   // cd from / to / gives you /
    | "/", _       -> $"/{dirName}"         // cd from / to x gives you /x
    | _, ".."      ->                       // cd up from /x gives you /
        let parentDir = Regex.Match(root, "^\/(.*)\/.*$").Groups.[1].Value
        $"/{parentDir}"
    | _, _         -> $"{root}/{dirName}"   // otherwise cd y from x gives you 'x/y' 

/// Takes list of rows and converts to list of directories
let parseRows (rows: string list) =
    /// Parse string for directory details and returns option of directory name
    let (|DirDetails|_|) str = 
        let result = Regex.Match(str, "dir (.*)")
        if result.Success then Some result.Groups.[1].Value
        else None

    /// Parse string for file details and returns option of fileName, fileSize
    let (|FileDetails|_|) str = 
        let result = Regex.Match(str, "(\d+) (.*)")
        if result.Success then Some(result.Groups.[2].Value, result.Groups.[1].Value)
        else None

    /// Parse string for change directory command and returns option of directory name
    let (|ChangeDirDetails|_|) str = 
        let result = Regex.Match(str, "\$ cd (.*)")
        if result.Success then Some result.Groups.[1].Value
        else None

    /// Create directory with given name and path
    let createDirectory name path =
        Directory { DirectoryName = name; Path = path; SubDirectories = []; Files = []; TotalSize = 0 }

    /// create file with given name and path
    let createFile name path (size: string) =
        File { FileName = name; Path = path; FileSize = int size }

    // Process rows at given path 
    let rec processRows path (results: FileOrDir list) rows =
        match rows with 
        // no more rows - return results
        | []            -> results
        // process head of list and recurse on tail
        | head :: tail  ->
            let path', results' =
                match head with
                // Cd command
                | ChangeDirDetails dirName          ->
                    let newDirPath = buildPath path dirName
                    newDirPath, results
                // Row is file - create new file and add to results
                | FileDetails(fileName, fileSize)   ->
                    let newFile = createFile fileName path fileSize
                    path, newFile :: results
                // Row is dir - create new directory and add to results
                | DirDetails subDirName             -> 
                    let newSubDir = createDirectory subDirName path
                    path, newSubDir :: results
                // Row is something else - just ignore and return path, results
                | _ -> path, results 

            processRows path' results' tail

    processRows "/" [] rows
    

/// Takes fileOrDirectory list an returns map of files and directories keyed by path
let buildPathMap fileOrDirList =
    fileOrDirList
    |> List.groupBy ( function
        | File { Path = path }      -> path
        | Directory { Path = path } -> path )
    |> Map.ofList

/// Takes fileOrDirList and returns tuple of files, directories
let getFilesAndDirs fileOrDirList =
    fileOrDirList
    |> List.fold ( fun (files, dirs) fod ->
        match fod with
        | File file     -> file :: files, dirs
        | Directory dir -> files, dir :: dirs ) ([],[])

// Build directory tree for given directory at given path, using fileAndDir map
let getDirectoryTree fileAndDirMap =
    let root = { DirectoryName = "/"; Path = "/"; SubDirectories = []; Files = []; TotalSize = 0}
    
    let rec processTree path directory =
        let files, subDirs = fileAndDirMap |> Map.find path |> getFilesAndDirs
        
        // Recurse on all subDirs to build up directory tree
        let subDirs' = 
            subDirs 
            |> List.map ( fun subDir -> 
                let subDirPath = buildPath path subDir.DirectoryName
                processTree subDirPath subDir )
        
        // Create new parent directory containing files and subdirs in path
        { directory with
            Files = files
            SubDirectories = subDirs'
            TotalSize = 0 }
    
    processTree "/" root

/// Takes directory and updates total size of dir and all sub dirs
let rec updateDirectorySizes directory =
    let totalFileSize = directory.Files |> List.map ( fun f -> f.FileSize ) |> List.sum
    if directory.SubDirectories.IsEmpty then 
        { directory with 
            TotalSize = totalFileSize }
    else
        let updatedSubDirs = directory.SubDirectories |> List.map updateDirectorySizes
        let totalSubDirSize = updatedSubDirs |> List.map ( fun d -> d.TotalSize ) |> List.sum
        { directory with 
            SubDirectories = updatedSubDirs
            TotalSize = totalFileSize + totalSubDirSize }

/// Filter directory, returning list of directories where predicate(d) is true
let filterDirectoryTree predicate directory =
    let rec recurse results dir =
        let results' = dir.SubDirectories |> List.collect (recurse results)
        if predicate dir then dir :: results' else results'
        
    recurse [] directory

// Part 1 - Find all of the directories with a total size of at most 100000. What is the sum of the total sizes of those directories?
readLines "./day7.input.txt"
|> parseRows
|> buildPathMap
|> getDirectoryTree
|> updateDirectorySizes
|> filterDirectoryTree ( fun dir -> dir.TotalSize <= 100000 )
|> List.map ( fun dir -> dir.TotalSize )
|> List.sum

// answer: 1845346

// Part 2 - Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the update. What is the total size of that directory?
let dirTree = 
    readLines "./day7.input.txt"
    |> parseRows
    |> buildPathMap
    |> getDirectoryTree
    |> updateDirectorySizes

let totalTreeSize = dirTree.TotalSize
let spaceNeeded = totalTreeSize - 40000000L

let result = 
    dirTree
    |> filterDirectoryTree ( fun dir -> dir.TotalSize >= spaceNeeded )
    |> List.sortBy ( fun dir -> dir.TotalSize )
    |> List.head

result.TotalSize    

// answer: 3636703