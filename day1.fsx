open System

let sample = 
    """1000
    2000
    3000

    4000

    5000
    6000

    7000
    8000
    9000

    10000"""

/// Read text from file at given path
let readText path =
    IO.File.ReadAllText path    

/// Takes string s, splits on separator and returns array of trimmed results
let splitAndTrim (separator: string) (s: string) =
    s.Split separator
    |> Array.map ( fun s -> s.Trim() )

/// Takes text input and returns array of elf inventories
let getInventories text = text |> splitAndTrim "\n\n"

/// Takes inventory and returns total calories in inventory
let getTotalCalories inventory = 
    inventory
    |> splitAndTrim "\n"
    |> Array.map int32
    |> Array.reduce (+)


/// Part 1 - Get max calories of all inventories
let caloryCounts = 
    readText "day1.input.txt"
    |> getInventories
    |> Array.map getTotalCalories

let maxCaloryCount = 
    caloryCounts 
    |> Array.max

/// Part 2 - sum of top 3 calory counts
let sumOfTop3CaloryCounts = 
    caloryCounts
    |> Array.sortDescending
    |> Array.take 3
    |> Array.reduce (+)
