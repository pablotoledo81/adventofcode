open System
open System.Text.RegularExpressions

let sample = 
  [ "    [D]    "
    "[N] [C]    "
    "[Z] [M] [P]"
    " 1   2   3 "
    ""
    "move 1 from 2 to 1"
    "move 3 from 1 to 3"
    "move 2 from 2 to 1"
    "move 1 from 1 to 2" ]    

type Stack = char list
type Movement = 
    { Quantity: int
      SourceStack: int
      DestStack: int }

// IO

/// Read text from file at given path
let readLines path =
    IO.File.ReadLines(path) |> List.ofSeq

// Pure functions

/// Takes list of rows and transforms into list of stacks
let getStacks (rows: string list): Stack list =
    let stringToList (s: string) = s.ToCharArray() |> List.ofArray
    let splitIndex = List.findIndex ( (=) "" ) rows
    let crateRows, _ = rows |> List.splitAt splitIndex
    let stackHeight = splitIndex - 1

    crateRows
    |> List.map stringToList        // convert rows to lists of chars
    |> List.transpose               // transpose rows to columns
    |> List.filter ( fun column ->  // filter out columns that are not stacks
        let lastChar = column.[stackHeight]
        let isStackNumber, _ = Int32.TryParse(lastChar.ToString())
        isStackNumber )
    |> List.map ( fun stack ->      // drop stack number and filter out empty crates from stack 
        stack
        |> List.take stackHeight
        |> List.filter ( fun c -> not (c = ' ') ) )

let getMovements (rows: string list) =
    let splitIndex = List.findIndex ( (=) "" ) rows + 1
    let _, movementRows = rows |> List.splitAt splitIndex
    let movementPattern = "^move (\d+) from (\d+) to (\d+)$"
    
    movementRows
    |> List.map ( fun row ->
        let result = Regex.Match(row, movementPattern)
        match result.Success with 
        | true  -> 
            { Quantity = int result.Groups.[1].Value
              SourceStack = int result.Groups.[2].Value
              DestStack = int result.Groups.[3].Value }
        | false -> failwith $"Unable to parse movement '{row}'" )

/// Move top crate from sourceStack to destStack a specified number of times
let rec moveCrate sourceStack destStack quantity =
    // base case - nothing left to move
    if quantity <= 0 then
        sourceStack, destStack
    // pop source stack, push contents onto dest stack and recurse
    else
        match sourceStack with
        | []  -> failwith "Cannot move from an empty stack"
        | sourceHead :: sourceTail  -> 
            let sourceStack' = sourceTail
            let destStack' = sourceHead :: destStack
            moveCrate sourceStack' destStack' (quantity - 1)

/// Move top crate from sourceStack to destStack a specified number of times,
/// this time retaining order of crates
let moveCrate2 sourceStack destStack quantity =
    let moveCrates, sourceStack' = sourceStack |> List.splitAt quantity
    let destStack' = moveCrates @ destStack
    sourceStack', destStack'         

/// Updates list of stacks at given index with new stack
let updateStacks stackIndex newStack stacks =
    stacks |> List.updateAt stackIndex newStack

/// Rearrange crates with given list of movements and list of stacks
let rearrangeCrates moveCrateFn (startingStacks: Stack list) (movements: Movement list) =
    (startingStacks, movements)
    ||> List.fold ( fun stacksResult move -> 
        let sourceIndex = move.SourceStack - 1
        let destIndex = move.DestStack - 1
        // Move crates and get updated source and dest stacks
        let sourceStack', destStack' = 
            moveCrateFn 
                stacksResult.[sourceIndex] 
                stacksResult.[destIndex] 
                move.Quantity
        
        // Update stacks result
        stacksResult
        |> updateStacks sourceIndex sourceStack'
        |> updateStacks destIndex destStack' ) 


// Part 1 - After the rearrangement procedure completes, what crate ends up on top of each stack?
let input = readLines "day5.input.txt"
let startingStacks = getStacks input
let movements = getMovements input

let topCrates = 
    movements
    |> rearrangeCrates moveCrate startingStacks
    |> List.map List.head

let answer = String.Join("", topCrates)  
// answer: VPCDMSLWJ  
        

// Part 2 - After the rearrangement procedure completes, what crate ends up on top of each stack?

let topCrates' = 
    movements
    |> rearrangeCrates moveCrate2 startingStacks
    |> List.map List.head

let answer2 = String.Join("", topCrates')    
// answer: TPWCGNCCG



