open System

let sample = 
    [|"2-4,6-8"
      "2-3,4-5"
      "5-7,7-9"
      "2-8,3-7"
      "6-6,4-6"
      "2-6,4-8"|]

// Domain Types
type Range =
    { Start: int
      End: int }

type Pair =
    { Assignment1: Range
      Assignment2: Range }

// IO

/// Read text from file at given path
let readLines path =
    IO.File.ReadLines(path) |> Array.ofSeq

// Pure functions

/// Takes array of lines and returns array of pairs 
let getPairs (lines: string array) =
    let getPair (s: string) =
        s.Split(",")

    let getAssignment (s: string) =
        let range = s.Split("-")
        { Start = int range.[0]
          End = int range.[1] }

    lines
    |> Array.map ( fun line ->
        let ranges =
            line
            |> getPair
            |> Array.map getAssignment
        
        { Assignment1 = ranges.[0]
          Assignment2 = ranges.[1] } )

/// Returns true if range r1 contains r2, else returns false 
let rangeContains (r1: Range) (r2: Range) =
    let r1Set = [r1.Start..r1.End] |> Set.ofSeq
    let r2Set = [r2.Start..r2.End] |> Set.ofSeq
    
    (r2Set, r1Set) ||> Set.isSubset

/// Returns true if r2 overlaps with r1, else returns false
let rangesOverlap (r1: Range) (r2: Range) =
    let r1Set = [r1.Start..r1.End] |> Set.ofSeq
    let r2Set = [r2.Start..r2.End] |> Set.ofSeq
    let intersection = (r1Set, r2Set) ||> Set.intersect 
    
    not intersection.IsEmpty
    

/// Returns true if assignment1 range contains assignment2 range or vici-versa
let pairHasContainingRange pair =
    rangeContains pair.Assignment1 pair.Assignment2
    || rangeContains pair.Assignment2 pair.Assignment1

/// Returns true if assignment2 overlaps assignment1
let pairHasOverlappingRange pair =
    rangesOverlap pair.Assignment1 pair.Assignment2

// Root

// Part 1 - In how many assignment pairs does one range fully contain the other?
let pairs = readLines "day4.input.txt" |> getPairs

pairs
|> Array.filter pairHasContainingRange
|> Array.length

// Answer: 605

// Part 2 - In how many assignment pairs do the ranges overlap?

pairs
|> Array.filter pairHasOverlappingRange
|> Array.length

// Answer: 914

